package com.ajax.service;

import org.springframework.http.ResponseEntity;

import com.ajax.dto.UserDto;

public interface UserService {

	public ResponseEntity<?> createUser(UserDto userDto);

	public ResponseEntity<?> userList();

	public ResponseEntity<?> userListWithStatus();

	public ResponseEntity<?> getUser(String id);

	public ResponseEntity<?> updateUser(UserDto userDto, String id);

	public ResponseEntity<?> removeUser(UserDto userDto, String id);

}
