package com.ajax.service;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ajax.dao.UserDao;
import com.ajax.dto.UserDto;
import com.ajax.model.User;
import com.ajax.response.ApiResponse;
import com.mongodb.client.model.FindOneAndUpdateOptions;

@Service
@Transactional
public class UserServiceImp implements UserService {

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private UserDao userDao;

	public ResponseEntity<?> createUser(UserDto userDto) {

		User user = new User();
		// if getting id is not null then that id is set in the object.

		if (userDto.getId() != null) {
			user.setId(userDto.getId());

		}

		user.setFirstname(userDto.getFirstname());
		user.setLastname(userDto.getLastname());
		user.setUsername(userDto.getUsername());
		user.setPassword(userDto.getPassword());

		User newUser = userDao.save(user);

		UserDto addUser = modelMapper.map(newUser, UserDto.class);
		System.out.println(">>>>>>>>>>>>>>>>>> " + addUser);

		return ResponseEntity.ok().body(new ApiResponse(HttpStatus.OK, "save user successFully.", addUser));

	}

	public ResponseEntity<?> userList() {
		List<User> user = userDao.findAll();

		return ResponseEntity.ok().body(new ApiResponse(HttpStatus.OK, "Get all data Successfully.", user));
	}

	public ResponseEntity<?> removeUser(String id) {
		userDao.delete(id);
		return ResponseEntity.ok().body("User delete successfully with this id : " + id);

	}

	@Override
	public ResponseEntity<?> getUser(String id) {

		User user = userDao.findOne(id);

		UserDto getUser = modelMapper.map(user, UserDto.class);

		return ResponseEntity.ok().body(new ApiResponse(HttpStatus.OK, "Data get Successfully", getUser));

	}

	@Override
	public ResponseEntity<?> updateUser(UserDto userDto, String id) {

		User user = userDao.findOne(id);
		System.out.println(user);

		if (user != null) {

			user.setFirstname(userDto.getFirstname());
			user.setLastname(userDto.getLastname());
			user.setUsername(userDto.getUsername());
			user.setPassword(userDto.getPassword());
			user.setStatus(userDto.getStatus());

		}

		User updateUser = userDao.save(user);
		UserDto update = modelMapper.map(updateUser, UserDto.class);

		return ResponseEntity.ok().body(new ApiResponse(HttpStatus.OK, "Data update Successfully", update));

	}

	@Override
	public ResponseEntity<?> removeUser(UserDto userDto, String id) {
		User user = userDao.findOne(id);
		if (user != null) {
			userDto.getStatus();
			user.setStatus(Boolean.FALSE);
		}

		User deleteUser = userDao.save(user);
		UserDto userInActive = modelMapper.map(deleteUser, UserDto.class);
		return ResponseEntity.ok().body(new ApiResponse(HttpStatus.OK, "Data deleted Successfully", userInActive));
	}

	@Override
	public ResponseEntity<?> userListWithStatus() {
		List<User> user = userDao.findAllWithStatus();

		return ResponseEntity.ok().body(new ApiResponse(HttpStatus.OK, "Get all data Successfully.", user));
	
	}
}
