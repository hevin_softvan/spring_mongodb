package com.ajax.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ajax.dto.UserDto;
import com.ajax.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping("/user")
	public ResponseEntity<?> createUser(@RequestBody UserDto userDto) {

		System.out.println(userDto);

		return this.userService.createUser(userDto);

	}

	@GetMapping("/user")
	public ResponseEntity<?> getAllUser() {

		return userService.userList();

	}
	
	@GetMapping("/user/withStatus")
	public ResponseEntity<?> getAllUserWithStatus() {

		return userService.userListWithStatus();

	}

	@GetMapping("/user/{id}") // for update data
	public ResponseEntity<?> getUserById(@PathVariable("id") String id) {
		return this.userService.getUser(id);

	}

	@PutMapping("/user/{id}") // for update data
	public ResponseEntity<?> updateUser(@RequestBody UserDto userDto, @PathVariable("id") String id) {
		return this.userService.updateUser(userDto, id);

	}

	@DeleteMapping("/user/{id}")
	public ResponseEntity<?> removeUser(@RequestBody UserDto userDto ,@PathVariable("id") String id) {

		return this.userService.removeUser(userDto,id);

	}

}