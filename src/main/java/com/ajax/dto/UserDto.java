package com.ajax.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class UserDto {

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@NotNull(message = "Please! Enter Firstname.")
	@Pattern(regexp = "^(?=.*[a-zA-Z]).{3,}$", message = "Firstname must be contain minimum three character.")
	private String firstname;

	@NotNull(message = "Please! Enter Lastname.")
	@Pattern(regexp = "^(?=.*[a-zA-Z]).{3,}$", message = "Lastname must be contain minimum three character.")
	private String lastname;

	@NotNull(message = "Please! Enter Username.")
	@Pattern(regexp = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$", message = "please! Enter Valid email address.")
	private String username;

	@NotNull(message = "Please! Enter password.")
	@Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,20}$", message = "Password must be contain uppercase and lowercase character, atleast minimum 8 and maximum 16 character and special character.")
	private String password;

	private Boolean status = true;

	private Date createdDateTime = new Date();

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Date getCreatedDateTime() {
		return createdDateTime;
	}

	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}

}
