package com.ajax.dao;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.ajax.dto.UserDto;
import com.ajax.model.User;

@Repository
public interface UserDao extends MongoRepository<User, String> {

	
	@Query("{'status':true}")
	List<User> findAllWithStatus();

}
