package com.ajax;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	static Logger log = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		log.info("Before start apllication: ");
		SpringApplication.run(Application.class, args);

	}
}
