package com.ajax.response;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ApiResponse {

	private int status;
	private String message;
	private Object data;

	@JsonIgnore
	private HttpStatus httpStatus;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public ApiResponse(HttpStatus status, String message, Object data) {
		super();
		this.httpStatus = status;
		this.status = status.value();
		this.message = message;
		this.data = data;

	}

	
}
